
function checkAuthorization(cb){
  // Dummy authorization to be replaced with api call to backend
  setTimeout(function() {
    cb(true);
  }, 400);
}

export {
  checkAuthorization
};
