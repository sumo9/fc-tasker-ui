var reports = {
  total: 40,
  perpage: 20,
  page: 0,
  data: [
    {
      id: 1,
      name: 'Recipe 1',
      status: 'progress',
      pid: 3,
      datetime: 1530009231
    },
    {
      id: 2,
      name: 'Recipe 2',
      status: 'success',
      pid: 2,
      datetime: 1530009231
    },
    {
      id: 3,
      name: 'Recipe 2',
      status: 'failed',
      pid: 1,
      datetime: 1530009231
    },
    {
      id: 4,
      name: 'Recipe 1',
      status: 'success',
      pid: 2,
      datetime: 1530009231
    },
    {
      id: 1,
      name: 'Recipe 1',
      status: 'failed',
      pid: 1,
      datetime: 1530009231
    },
    {
      id: 3,
      name: 'Recipe 2',
      status: 'failed',
      pid: 2,
      datetime: 1530009231
    },
    {
      id: 4,
      name: 'Recipe 1',
      status: 'success',
      pid: 6,
      datetime: 1530009231
    },
    {
      id: 1,
      name: 'Recipe 1',
      status: 'failed',
      pid: 4,
      datetime: 1530009231
    }
  ]
};

export default reports;
