import React from 'react';
import { Route , Redirect } from "react-router-dom";
import { checkAuthorization } from "../auth/auth";

class PrivateRoute extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      loading: true,
      isAuthenticated: false
    }
  }
  componentDidMount(){
    checkAuthorization((res) => {
      this.setState({
        loading: false,
        isAuthenticated: res
      })
    });
  }

  render(){
    let { component:Component,  ...rest } = this.props;
    if(this.state.loading){
      return <div>Loading...</div>;
    } else {

      // Return a Route component that either renders a component OR a redirect depending whether user is authenticated or not resp.
      return (
        <Route
          {...rest}
          render={
            props => (
              <div>
                {
                  this.state.isAuthenticated ?
                  <Component {...rest}/> :
                  <Redirect to={{
                    pathname: '/login',
                    state: { from: this.props.location }
                  }}/>
                }
              </div>
            )
          }
        />
      )
    }
  }

}

export default PrivateRoute;
