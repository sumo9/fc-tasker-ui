import React from 'react';
import { BrowserRouter as Router, Route, Redirect} from "react-router-dom";

import PrivateRoute from './private-route';
import Dashboard from "../components/dashboard";
import Reports from "../components/reports";

// Login page Route
import Login from "../components/login";


const basePathRedirect = {
  pathname : '/dashboard'
}


const AppRoutes = () => (
  <Router>
    <div>
      <Route path='/login' component={Login}/>
      <Route exact path='/' render={props => (<Redirect to={basePathRedirect}/>)}/>
      <PrivateRoute path='/dashboard' component={Dashboard}/>
      <PrivateRoute path='/reports' component={Reports}/>
    </div>
  </Router>
);


export default AppRoutes;
