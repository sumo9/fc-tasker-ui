import React from 'react';
import { Route, Switch } from 'react-router-dom';
// Recipe
import AddRecipe from "../components/recipes/add-recipe";
import ViewRecipe from "../components/recipes/view-recipes";
//Action
import AddAction from "../components/actions/add-action";
import ViewAction from "../components/actions/view-actions";
import EditAction from "../components/actions/edit-action";
import NoMatch from '../components/utils/no-match';
// 404 No match


let dashboardRoutes = path => (
  <Switch>
    <Route exact path={`${path}`} render={(props)=>(<div></div>)}/>
    <Route exact path={`${path}/recipes`} component={AddRecipe}/>  
    <Route exact path={`${path}/recipes/add`} component={ViewRecipe}/>
    <Route exact path={`${path}/actions`} component={ViewAction}/>  
    <Route exact path={`${path}/actions/add`} component={AddAction}/>
    <Route exact path={`${path}/actions/edit/:id`} component={EditAction}/>
    <Route component={NoMatch}/>
  </Switch>
)

export default dashboardRoutes;