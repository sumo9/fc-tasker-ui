import React from 'react';
import { Route, Switch } from 'react-router-dom';

import AllReports from "../components/reports/all-reports";

// 404 No match
import NoMatch from '../components/utils/no-match';


let reportRoutes = path => (
  <Switch>
    <Route exact path={`${path}`} component={AllReports}/>
    <Route component={NoMatch}/>
  </Switch>
)

export default reportRoutes;
