import React from "react";
import Wrapper from './utils/wrapper';
import DashboardRoutes from '../routes/dashboard-routes';
class Dashboard extends React.Component{
  constructor(props){
    super(props);
  }
  render() {

    // Renders the dashboard content inside the wrapper component
    // pageTitle sets the page title in the appbar inside wrapper component
    return (
      <Wrapper pageTitle="Dashboard">
        {DashboardRoutes(this.props.path)}
      </Wrapper>
    )
  }
}

export default Dashboard;
