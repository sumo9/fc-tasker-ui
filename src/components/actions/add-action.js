import React from 'react';
import ActionForm from './action-form';

class AddAction extends React.Component{
  constructor(props){
    super(props);
  }
  render(){
    return (
      <ActionForm 
        onActionData={(data) => console.log('AddAction',data)}
        submitBtnText='Create Action'/>
    )
  }
}


export default AddAction;