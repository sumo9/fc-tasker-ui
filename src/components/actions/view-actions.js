import React from "react";
import PropTypes from 'prop-types';
import {
   Paper,
   Table,
   TableHead,
   TableRow,
   TableCell,
   TableBody,
   Typography,
   Button,
   withStyles
} from "@material-ui/core";

import { Edit as EditIcon,Delete as DeleteIcon  } from "@material-ui/icons";
const perpage = 3;


function dummyData(){
  return [
    { id: 1, title: 'Title', type: 'webservice', url: 'https://google.com'},
    { id: 2, title: 'TitleA', type: 'webservice', url: 'https://google.com'},
    { id: 3, title: 'TitleB', type: 'webservice', url: 'https://google.com'},
    { id: 4, title: 'TitleC', type: 'webservice', url: 'https://google.com'},
    { id: 5, title: 'TitleD', type: 'webservice', url: 'https://google.com'}
  ]
}

const styles = theme => ({
  actionButton: {
    marginLeft: 3,
    marginRight: 3,
  }
})

class ViewAction extends React.Component {
  
  constructor(props){
    super(props);
    
  }

  DataRow(rowData){
    const { classes } = this.props;
    return (
      <TableRow>
        <TableCell>{rowData.id}</TableCell>
        <TableCell>{rowData.title}</TableCell>
        <TableCell>{rowData.type}</TableCell>
        <TableCell>{rowData.url}</TableCell>
        <TableCell>
          <Typography align='center'>
            <Button mini={true}  variant='raised' size='medium' className={classes.actionButton}>
              <EditIcon />
            </Button> 
            <Button mini={true}  variant='raised' color="secondary" size='medium' className={classes.actionButton}>
              <DeleteIcon />
            </Button> 
          </Typography>
        </TableCell>
      </TableRow>
    )
  }

  DataTable(data){
    return (
      <Paper>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Action Id</TableCell>
              <TableCell>Action Title</TableCell>
              <TableCell>Action Type</TableCell>
              <TableCell>Action URL</TableCell>
              <TableCell>
                <Typography align='center'>----</Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {
              data.map(d => {
                return this.DataRow(d)
              })
            }
          </TableBody>
        </Table>
      </Paper>
    )
  }

 
  render(){
    
    return (
      <div>
        {this.DataTable(dummyData())}
      </div>
    )
  }
}

export default withStyles(styles)(ViewAction);