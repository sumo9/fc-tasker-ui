import React from 'react';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import KeyValueForm from '../utils/key-value-form';

const actionTypes = [
  { value : 'webservice',label: 'Web Service'}
];

const reqMethods = [
  { value: 'GET' , label: 'GET' },
  { value: 'POST', label: 'POST'}
];

class ActionForm extends React.Component {
  constructor(props){
    super(props);
    // Placeholder value headers and body
    let request = {
      req_url : props.url || '',
      req_method : props.method || '',
      req_urlencode: props.urlencode || true,
      req_body : props.req_body || [],
      req_response: props.req_response ||  [],
      req_headers: props.req_headers || [] 
    }
    this.state = {
      title : props.title || '',
      type : props.type || '',
      ...request,
      error: ''
    }
  }

  showError(error){
    this.setState({
      error: error
    })
  }
  /**
   * Transforms array of key value pair to a json object
   * @param {Array} keyValueArr -> [['key1', 'value1'], ['key2', 'value2']]
   */
  transformToObject(keyValueArr, checkValueExist = false){
    let obj = {}, isValid;
    isValid = keyValueArr.every(keyValPair => {
      if(!Array.isArray(keyValPair)) return false;
      if(!keyValPair[0]) return false;
      if(checkValueExist && !keyValPair[1]) return false;
      obj[keyValPair[0]] = keyValPair[1];
      return true; 
    });
    return isValid && obj;
  }

  handleSubmit(){
    let state = this.state,
      headerObj = {},
      requestBodyObj = {},
      requestResponseObj = {},
      validRegexTest = /(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+/gm;
    this.showError('');
    if(!state.title || state.title.length === 0) return this.showError('Invalid title');
    if(!state.type || state.title.length === 0) return this.showError('Invalid type');
    if(!state.req_url || state.req_url.length === 0 || !validRegexTest.test(state.req_url))
      return this.showError('Invalid url');
    if(!state.req_method || state.req_method.length === 0) return this.showError('Please select a method');
    /**
     * Check if headers exists then it must be valid , 
     * otherwise headers are optional
     */
    if(state.req_headers || state.req_headers.length){
      let hObj = this.transformToObject(state.req_headers, true);
      if(!hObj) return this.showError('Invalid headers');
      headerObj = hObj; 
    }
    
    /**
     * Check request body must exists (mandatory)
     */
    if(!Array.isArray(state.req_body) || state.req_body.length === 0){
      return this.showError('Request body is mandatory');
    } else {
      let rbObj = this.transformToObject(state.req_body, true);
      if(!rbObj) return this.showError('Invalid request body');
      requestBodyObj = rbObj;
    }
    /**
     * Check request response body , if it exists it should valid
     * value is optional here
     */
    if(Array.isArray(state.req_response) && state.req_response.length){
      let rrObj = this.transformToObject(state.req_response);
      if(!rrObj) return this.showError('Invalid response object');
      requestResponseObj = rrObj;
    }
      
    let actionData = {
      title : state.title,
      type: state.type,
      request:{
        url: state.req_url,
        method: state.req_method,
        urlencode: state.req_urlencode,
        body: requestBodyObj,
        headers: headerObj
      },
      response:requestResponseObj
    };
    this.props.onActionData && this.props.onActionData(actionData);
    return;
  }


  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  handleRadio = name => event => {
    this.setState({
      [name]: event.target.checked
    })
  };

  getWebServiceForm(){
    return (
      <div>
        <Typography variant="subheading"> Web Service Form </Typography>
        <TextField
          id='requrl'
          label='URL'
          value={this.state.req_url}
          onChange={this.handleChange('req_url')}
          fullWidth
        />
        <TextField
          id='reqmethod'
          label='Method'
          value={this.state.req_method}
          onChange={this.handleChange('req_method')}
          fullWidth
          select
          margin='normal'
        >
          {
            reqMethods.map(option => (
              <option key={option.value} value={option.value}>
                {option.label}
              </option>
            ))
          }
        </TextField>
        <FormControlLabel
          control={
            <Switch 
              checked={this.state.req_urlencode}
              id='url_encode'
              onChange={this.handleRadio('req_urlencode')}
            />
          }
          label="Encode URL"
        />
        <KeyValueForm 
          onChange={(items) => { 
            this.setState({
              req_headers: items
            })
          }} 
          title='Headers'
          itemTitle='header'
          defaultItems={this.state.req_headers}/>
        <KeyValueForm 
          onChange={(items) => { 
            this.setState({
              req_body: items
            })
          }} 
          title='Request Body'
          itemTitle='field'
          defaultItems={this.state.req_body}/>
        <KeyValueForm 
          onChange={(items) => { 
            this.setState({
              req_response: items
            })
          }} 
          title='Response Body'
          itemTitle='field'
          defaultItems={this.state.req_response}/>
      </div>
    )
  }
  

  getForm(){
    return (
      <div>
        <TextField
          id='title'
          label='Title'
          value={this.state.title}
          onChange={this.handleChange('title')}
          fullWidth
          margin='normal'
        />
        <TextField
          id='type'
          label='Type'
          value={this.state.type}
          select
          fullWidth
          onChange={this.handleChange('type')}
          margin='normal'
        >
          {actionTypes.map(option => 
            (
              <option key={option.value} value={option.value}>
                {option.label}
              </option>
            )
          )}
        </TextField>
        <br/>
        {this.getWebServiceForm()}
        <Grid container direction='column' alignItems='flex-end'>
          <Grid item xs={4}>
            <Button variant='contained' color='primary' onClick={() => this.handleSubmit()}>
              {this.props.submitBtnText || ' Submit '}
            </Button>
            <Typography color='error'>{this.state.error || ''}</Typography>
          </Grid>
          {/* <Grid item xs={12}></Grid> TODO: Find other way to line break */}
        </Grid>
        
      </div>
    )
  }

  render(){
    return (
      <div>
        <Grid container>
          <Grid item xs={12} md={6}>
            <Card>
              <CardContent>
                {this.getForm()}
              </CardContent>
            </Card>
          </Grid>
        </Grid>
      </div>    
    )
  }
}

export default ActionForm;