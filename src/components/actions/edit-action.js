import React, { Component } from 'react';
import ActionForm from './action-form';
class EditAction extends Component {
  constructor(props) {
    super(props);
    this.state = { 

    };
  }
  /**
   * Returns array of array for key value pair 
   * @param {Object} json one level key value pair
   */
  transformJsonToArray(json){
    let keyValArr = [];
    if(!json) return keyValArr;
    Object.keys(json).forEach(key => {
      keyValArr.push([key, json[key] || '']);
    });
    return keyValArr;
  }

  render() { 
    return ( 
      <div>
        <div>Edit Action</div>
        <ActionForm 
          title='Just a title'
          type='webservice' 
          url='aaa.com' 
          method='GET' 
          req_body={this.transformJsonToArray({key : 'value', key1:'value2'})}
          req_headers={this.transformJsonToArray({key : 'value', key1:'value2'})}
          req_response={this.transformJsonToArray({key : 'value', key1:'value2'})}
          submitBtnText='Save Action'/>
      </div>
    )
  }
}
 
export default EditAction;