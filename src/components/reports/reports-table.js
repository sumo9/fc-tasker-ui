import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import Button from '@material-ui/core/Button';
import VisibilityIcon from "@material-ui/icons/Visibility";
import CheckIcon from "@material-ui/icons/Check";
import CloseIcon from "@material-ui/icons/Close";
import SyncIcon from "@material-ui/icons/Sync";

import Tooltip from '@material-ui/core/Tooltip';

const styles ={
  root: {
    width: '100%',
    overflowX: 'auto',
    marginTop: 30,
    marginBottom: 30
  },
  table: {
    minWidth: 700,
  },
  progressIcon: {
    color: 'orange'
  },
  failedIcon: {
    color: 'red'
  },
  successIcon: {
    color: 'green'
  },
  processID: {
    width: 75
  },
  status: {
    width: 50
  },
  view: {
    width: 150
  }
};

function getStatusIcon(status, classes) {
  switch(status) {
    case 'progress':
      return <SyncIcon className={classes.progressIcon}/>;

    case 'failed':
      return <CloseIcon className={classes.failedIcon}/>;

    case 'success':
      return <CheckIcon className={classes.successIcon}/>;
  }
}

class AllReports extends React.Component {
  constructor(props){
    super(props);
  }

  render(){
    const { classes } = this.props,
      reportData = this.props.reportData;

    return (
      <div className={classes.root}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell numeric>Recipe ID</TableCell>
              <TableCell>Date-Time Run</TableCell>
              <TableCell>Recipe Name</TableCell>
              <TableCell>Status</TableCell>
              <TableCell></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
          {reportData.map(process => {
            var t = new Date(process.datetime),
              formatted = t.toLocaleString(),
              statusIcon = getStatusIcon(process.status, classes);

            return (
              <TableRow key={process.id + '-' + process.pid}>
                <TableCell numeric className={classes.processID}># {process.id}</TableCell>
                <TableCell>{formatted}</TableCell>
                <TableCell>{process.name}</TableCell>
                <TableCell className={classes.status}>
                  <Tooltip title={process.status}>
                    {statusIcon}
                  </Tooltip>
                </TableCell>
                <TableCell className={classes.view}>
                  <Button href={'/reports/' + process.id}>
                    <VisibilityIcon />
                    View Details
                  </Button>
                </TableCell>
              </TableRow>
            )
          })}
          </TableBody>
        </Table>
      </div>
    )
  }
}

AllReports.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(AllReports);
