import React from 'react';
import reports from "../../dummyData/reports-all";
import ReportTable from './reports-table';
import Pagination from '../utils/pagination';
import MultipleSelect from '../utils/multi-select';

const perpage = 3;

class AllReports extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      page: 1,
    };
  }

  // API Call to get data
  getReports() {
    const page = this.state.page,
      total = this.state.total || reports.data.length,
      offset = (page - 1) * perpage;
    this.state.total = this.state.total || reports.data.length;
    return reports.data.slice(offset, offset + perpage);
  }

  getRecipes() {
    var filterValues = {}, ids = [], names = [];
    const recipes = [
      {
        id: 1,
        name: 'Recipe 1'
      },
      {
        id: 2,
        name: 'Recipe 2'
      },
      {
        id: 5,
        name: 'Recipe 5'
      },
      {
        id: 3,
        name: 'Recipe 3'
      },
      {
        id: 4,
        name: 'Recipe 4'
      }
    ];

    recipes.forEach(recipe => {
      ids.push(recipe.id);
      names.push(recipe.name);
    });

    filterValues = {
      'ids': ids,
      'names': names
    }

    return filterValues;
  }

  getStatuses() {
    var filterValues = {},
      ids = ['success', 'failure', 'progress'],
      names = ['Success', 'Failure', 'Progress'];
    filterValues = {
      'ids': ids,
      'names': names
    };
    return filterValues;
  }

  render(){
    const { classes } = this.props,
      filterValuesRecipe = this.getRecipes(),
      filterValuesStatus = this.getStatuses(),
      reportData = this.getReports();

    return (
      <div>
        <MultipleSelect label="Filter by Recipe(s)" values={filterValuesRecipe} />
        <MultipleSelect label="Filter by Status" values= {filterValuesStatus} />

        <ReportTable reportData={reportData}/>

        <Pagination
          total={this.state.total}
          perpage={perpage}
          onChange={(current) => this.setState({ page: current })}
        />
      </div>
    )
  }
}

export default AllReports;
