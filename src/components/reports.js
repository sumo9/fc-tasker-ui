import React from "react";
import Wrapper from './utils/wrapper';
import ReportRoutes from '../routes/reports-routes';
class Reports extends React.Component{
  constructor(props){
    super(props);
  }
  render() {

    // Renders the reports content inside the wrapper component
    // pageTitle sets the page title in the appbar inside wrapper component
    return (
      <Wrapper pageTitle="Reports">
        {ReportRoutes(this.props.path)}
      </Wrapper>
    )
  }
}

export default Reports;
