import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import ListItemText from '@material-ui/core/ListItemText';
import Select from '@material-ui/core/Select';
import Checkbox from '@material-ui/core/Checkbox';
import Chip from '@material-ui/core/Chip';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 300,
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: theme.spacing.unit / 4,
  },
});

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

class MultipleSelect extends React.Component {
  state = {
    values: [],
  };

  handleChange = event => {
    this.setState({
      values: event.target.value
    });
  };

  getValueName(filterValues, value) {
    var index = filterValues.ids.indexOf(value);
    return filterValues.names[index];
  }

  render() {
    const { classes, theme } = this.props,
      filterValues = this.props.values;

    return (
      <div className={classes.root}>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="select-multiple-checkbox">{this.props.label || 'Filter by'}</InputLabel>
          <Select
            multiple
            value={this.state.values}
            onChange={this.handleChange}
            input={<Input id="select-multiple-checkbox" />}
            renderValue={selected => (
              <div className={classes.chips}>
                {selected.map((value, index) => <Chip key={index} label={this.getValueName(filterValues, value)} className={classes.chip} />)}
              </div>
            )}
            MenuProps={MenuProps}
          >
            {filterValues.ids.map(value => (
              <MenuItem key={value} value={value}>
                <Checkbox checked={this.state.values.indexOf(value) > -1} />
                <ListItemText primary={this.getValueName(filterValues, value)} />
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </div>
    );
  }
}

MultipleSelect.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired
};



export default withStyles(styles, { withTheme: true })(MultipleSelect);

