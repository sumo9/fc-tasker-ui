import React from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';

class KeyValueRow extends React.Component{

  constructor(props){
    super(props);
    this.onKeyChange = this.onKeyChange.bind(this);
    this.onValueChange = this.onValueChange.bind(this); 
    this.onRemove = this.onRemove.bind(this);
  }

  onKeyChange(e){
    this.props.onKeyChange(e.target.value);
  }

  onValueChange(e){
    this.props.onValueChange(e.target.value);
  }

  onRemove(){
    this.props.onRemove();
  }

  render(){
    return (
      
      <Grid container spacing={8}>
        <Grid xs={5} item >
          <TextField
            label={this.props.keyLabel || 'Key'}
            fullWidth
            value={this.props.keyItem}
            onChange={this.onKeyChange}
            />
        </Grid>
        <Grid xs={5} item >
          <TextField
            label={this.props.valueLabel || 'Value'}
            fullWidth
            value={this.props.valueItem}
            onChange={this.onValueChange}
          />      
        </Grid>
        <Grid xs={2} item>
          <Button variant='contained' onClick={this.onRemove}> 
              Remove 
          </Button>
        </Grid>
      </Grid>
    )
  }
}

class KeyValueForm extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      items: (this.props.defaultItems && this.props.defaultItems.slice())|| [] 
    }

    this.style = {
      defStyle : {
        marginBottom: '10px'
      },
      finStyle : {
        marginTop: '3px',        
        paddingLeft : '20px',
        paddingRight : '20px',
        paddingBottom: '10px'
      }
    }
  }

  addItem(){
    let prevItemsState = this.state.items.slice();
    prevItemsState.push(['', '']); 
    this.setState({
      items : prevItemsState
    });
  }

  
  notifyChange(){
    this.props.onChange && this.props.onChange(this.state.items.slice());
  }

  onKeyChange(index, value){
    let prevItemsState = this.state.items.slice();
    prevItemsState[index][0] = value;
    this.setState({
      items: prevItemsState
    }, () => this.notifyChange());
  }

  onValueChange(index, value){
    let prevItemsState = this.state.items.slice();
    prevItemsState[index][1] = value;
    this.setState({
      items: prevItemsState
    }, () => this.notifyChange())
  }

  removeItem(index){
    let prevItemsState = this.state.items.slice();
    prevItemsState.splice(index, 1);
    this.setState({
      items: prevItemsState
    }, () => this.notifyChange());
  }

  render(){
    return ( 
      <div >
        <Grid container >
          <Grid item xs={12}>
            <Typography paragraph={true} variant="subheading">{this.props.title}</Typography>
          </Grid>
          <Grid item xs={12}>
            <div style={this.state.items.length > 0 ? this.style.finStyle : this.style.defStyle}>
              {
                this.state.items.map((item, index)=> {
                  return (
                    <KeyValueRow 
                      key={index}
                      keyItem={item[0]}
                      valueItem={item[1]}
                      onKeyChange={(value) => this.onKeyChange(index, value)}
                      onValueChange={(value) => this.onValueChange(index, value)}
                      onRemove={() => this.removeItem(index)}/>
                  )
                })
              }
            </div>
          </Grid>
          <Button variant="outlined" color="primary" onClick={()=>this.addItem()}>
            Add {this.props.itemTitle || 'Item'}
          </Button>
        </Grid>
      </div>
      
    )
  }
}

export default KeyValueForm;