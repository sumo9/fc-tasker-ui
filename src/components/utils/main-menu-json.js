import HomeIcon from '@material-ui/icons/Home';
import ColorLensIcon from '@material-ui/icons/ColorLens';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import EditIcon from '@material-ui/icons/Edit';
import ListIcon from '@material-ui/icons/List';
import ReportIcon from '@material-ui/icons/Report';

// The main menu items is defined here.
// Hierarchy: 1 level
var mainMenu = [
  {
    "id": 1,
    "title": "Home",
    "icon": HomeIcon,
    "to": "/"
  },
  {
    "id": 2,
    "title": "Recipes",
    "icon": ColorLensIcon,
    "items": [
      {
        "id": 1,
        "title": "View Recipes",
        "icon": ListIcon,
        "to": "/dashboard/recipes"
      },
      {
        "id": 2,
        "title": "Create Recipe",
        "icon": EditIcon,
        "to": "/dashboard/recipes/add"
      }
    ]
  },
  {
    "id": 3,
    "title": "Actions",
    "icon": CheckCircleIcon,
    "items": [
      {
        "id": 1,
        "title": "View Actions",
        "icon": ListIcon,
        "to": "/dashboard/actions"
      },
      {
        "id": 2,
        "title": "Create Action",
        "icon": EditIcon,
        "to": "/dashboard/actions/add"
      }
    ]
  },
  {
    "id": 4,
    "title": "Reports",
    "icon": ReportIcon,
    "to": "/reports"
  }
];

export default mainMenu;
