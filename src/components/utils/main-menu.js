import React from 'react';
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";
import { withStyles } from '@material-ui/core/styles';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

import Tooltip from '@material-ui/core/Tooltip';

import MainMenu from "./main-menu-json";

// Style definitions for the main menu.
const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: 0,
  },
});

class NestedList extends React.Component {
  state = { open: true };

  // Handles the opening and closer of the nested menus identified by the parent menu title.
  handleClick = (e) => {
    this.setState({ [e]: !this.state[e] });
  };

  render() {
    const { classes } = this.props,
      items = MainMenu;

    // Contains the main menu List.
    // Each menu item lives inside a ListItem
    // Each nested menu dropdown lives inside a Collapse
    //  ListItem: contains the
    //    - menu item icon inside a tooltip
    //    - menu item text
    //  Collapse: contains a List containing ListItems

    return (
      <div className={classes.root}>
        <List component="ul">
          {items.map(menuItem => {
            return (
              <li key={"wrapper-"+ menuItem.id}>
                <ListItem key={menuItem.id} onClick={this.handleClick.bind(this, menuItem.title)} {...(menuItem.to ?
                  {component: (Link), to: menuItem.to } : {})} button>
                  <Tooltip title={menuItem.title}>
                    <ListItemIcon>
                      <menuItem.icon />
                    </ListItemIcon>
                  </Tooltip>
                    <ListItemText inset primary={menuItem.title} />
                  {
                    menuItem.items ? (this.state[menuItem.title] ? <ExpandLess /> : <ExpandMore />) : ''
                  }
                </ListItem>
                {
                  menuItem.items ?
                    (
                      <Collapse component="ul" key={"submenu-" + menuItem.id} in={this.state[menuItem.title]} timeout="auto" unmountOnExit>
                        <List component="li">
                        {menuItem.items.map(item => {
                          return (
                            <ListItem key={item.id} {...(item.to ?
                              {component: (Link), to: item.to } : {})} button className={classes.nested}>
                              <Tooltip title={item.title}>
                                <ListItemIcon>
                                  <item.icon />
                                </ListItemIcon>
                              </Tooltip>
                              <ListItemText inset primary={item.title} />
                            </ListItem>
                          )
                        })}
                        </List>
                      </Collapse>
                    ) : null
                }
              </li>
            )
          })}
        </List>
      </div>
    );
  }
}

NestedList.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(NestedList);
