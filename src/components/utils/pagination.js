import React from 'react';
import PropTypes from 'prop-types';
import ClassNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import LeftIcon from "@material-ui/icons/PlayArrow";

const styles = theme => ({
    pager: {
      textAlign: 'center',
      marginTop: 30
    },
    pageButton: {
      marginRight: 20,
      marginLeft: 20,
      borderRadius: '50%',
      background: 'rgba(0, 0, 0, 0.05)',
      minWidth: 40,
      minHeight: 40,
      padding: 0
    },
    pagerButtonLeft: {
      transform: 'rotate(180deg)'
    }
  });

class AllReports extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      page: 1,
    };

    this.handleClick = this.handleClick.bind(this);
  }

  // Handler for pagination
  handleClick(pager) {
    var current = this.state.page,
      newPage;

    if (pager === 'previous' && current > 1) {
      newPage = current - 1;
    } else if(pager === 'next') {
      newPage = current + 1;
    }

    this.props.onChange(newPage);
    this.setState({
      page: newPage,
    });

  }

  getPager(pager, classes) {
    if (pager === 'previous') {
      if (this.state.page > 1) {
        return (
          <Button mini onClick={() => this.handleClick('previous')} className={ClassNames(classes.pageButton, classes.pagerButtonLeft)}>
            <LeftIcon />
          </Button>
        );
      } else {
        return (
          <Button mini disabled className={ClassNames(classes.pageButton, classes.pagerButtonLeft)}>
            <LeftIcon />
          </Button>
        )
      }
    } else if(pager === 'next') {
      if(this.state.page < Math.ceil(this.props.total / this.props.perpage)) {
        return (
          <Button mini onClick={() => this.handleClick('next')} className={classes.pageButton}>
            <LeftIcon />
          </Button>
        );
      } else {
        return (
          <Button mini disabled className={classes.pageButton}>
            <LeftIcon />
          </Button>
        )
      }
    }
  }

  render(){
    const { classes } = this.props;

    return (
        <div className={classes.pager}>
          {this.getPager('previous', classes)}
            <span>Page {this.state.page}</span>
          {this.getPager('next', classes)}
        </div>
    )
  }
}

AllReports.propTypes = {
  classes: PropTypes.object.isRequired,
  onChange: PropTypes.func
}

export default withStyles(styles)(AllReports);
