import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import AppBarSidebar from './appbar-sidebar';

const drawerCollapsedWidth = 73, drawerWidth = 240,
  styles = theme => ({
    appBar: {
      marginLeft: drawerCollapsedWidth,
    },
    toolbar: {
      ...theme.mixins.toolbar,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing.unit * 3
    },
  });
class Wrapper extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { classes, theme } = this.props;

    // 2 sections -
    //    - appbar with toolbar and page title + drawer with main menu
    //    - main section to render the actual page content
    //  Main section: contains
    //    - empty div with styles to adjust the main content wrt the appbar and collapsed drawer
    //    - actual page content

    return (
      <div>
        <AppBarSidebar pageTitle={this.props.pageTitle} />
        <main className={classNames(classes.content, classes.appBar)}>
          <div className={classes.toolbar} />
          { this.props.children }
        </main>
      </div>
    );
  }
}

Wrapper.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired
};

export default withStyles(styles, { withTheme: true })(Wrapper);
